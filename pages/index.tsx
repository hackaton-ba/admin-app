import AppLayout from '@/components/common/Layout';
import Image from 'next/image'
import { BiCar } from 'react-icons/bi';
import { HiDocumentDuplicate } from 'react-icons/hi';

export default function Index() {
  return (
    <div id="home">

      <nav className="text-sm font-semibold mb-6" aria-label="Breadcrumb">
        <ol className="list-none p-0 inline-flex">
          <li className="flex items-center text-blue-500">
            <a href="#" className="text-gray-700">Domov</a>
            {/* <svg className="fill-current w-3 h-3 mx-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" /></svg> */}
          </li>
          {/* <li className="flex items-center">
            <a href="#" className="text-gray-600">Dashboard</a>
          </li> */}
        </ol>
      </nav>

      <div className="lg:flex justify-between items-center mb-6">
        <p className="text-2xl font-semibold mb-2 lg:mb-0">Dobrý deň</p>
        <button className="bg-blue-500 hover:bg-blue-600 focus:outline-none rounded-lg px-6 py-2 text-white font-semibold shadow">Záznamy</button>
      </div>

      <div className="flex flex-wrap -mx-3 mb-20">

        <div className="w-1/2 xl:w-1/3 px-3">
          <div className="w-full bg-white border-0 text-blue-400 rounded-lg flex items-center p-6 mb-6 xl:mb-0 shadow-lg">

            <HiDocumentDuplicate
              className="w-16 h-16 fill-current mr-4 hidden lg:block"
            />

            <div className="text-gray-700">
              <p className="font-semibold text-3xl">237</p>
              <p className=''>Počet nových žiadostí</p>
            </div>

          </div>
        </div>

        <div className="w-1/2 xl:w-1/3 px-3">
          <div className="w-full bg-white border-0 text-blue-400 rounded-lg flex items-center p-6 mb-6 xl:mb-0 shadow-lg">
            <BiCar
              className="w-16 h-16 fill-current mr-4 hidden lg:block"
            />

            <div className="text-gray-700">
              <p className="font-semibold text-3xl">177</p>
              <p className=''>Dnešný počet návštev</p>
            </div>
          </div>
        </div>

        <div className="w-1/2 xl:w-1/3 px-3">
          <div className="w-full bg-white border-0 text-blue-400 rounded-lg flex items-center p-6 shadow-lg">
            <svg className="w-16 h-16 fill-current mr-4 hidden lg:block" viewBox="0 0 20 20">
              <path d="M14.999,8.543c0,0.229-0.188,0.417-0.416,0.417H5.417C5.187,8.959,5,8.772,5,8.543s0.188-0.417,0.417-0.417h9.167C14.812,8.126,14.999,8.314,14.999,8.543 M12.037,10.213H5.417C5.187,10.213,5,10.4,5,10.63c0,0.229,0.188,0.416,0.417,0.416h6.621c0.229,0,0.416-0.188,0.416-0.416C12.453,10.4,12.266,10.213,12.037,10.213 M14.583,6.046H5.417C5.187,6.046,5,6.233,5,6.463c0,0.229,0.188,0.417,0.417,0.417h9.167c0.229,0,0.416-0.188,0.416-0.417C14.999,6.233,14.812,6.046,14.583,6.046 M17.916,3.542v10c0,0.229-0.188,0.417-0.417,0.417H9.373l-2.829,2.796c-0.117,0.116-0.71,0.297-0.71-0.296v-2.5H2.5c-0.229,0-0.417-0.188-0.417-0.417v-10c0-0.229,0.188-0.417,0.417-0.417h15C17.729,3.126,17.916,3.313,17.916,3.542 M17.083,3.959H2.917v9.167H6.25c0.229,0,0.417,0.187,0.417,0.416v1.919l2.242-2.215c0.079-0.077,0.184-0.12,0.294-0.12h7.881V3.959z"></path>
            </svg>

            <div className="text-gray-700">
              <p className="font-semibold text-3xl">31</p>
              <p className=''>Počet aktuálnych vymáhani</p>
            </div>
          </div>
        </div>

      </div>

      <div className="flex flex-wrap -mx-3">

        <div className="w-full xl:w-1/3 px-3">
          <p className="text-xl font-semibold mb-4">Najnovšie žiadosti</p>

          <div className="w-full bg-white border-0 rounded-lg p-4 mb-8 xl:mb-0 shadow-lg">
            <RequestCard
              name="Janko Mokrý"
              date="12.12.2020"
            />
            <RequestCard
              name="Janko Mokrý"
              date="12.12.2020"
            />
            <RequestCard
              name="Janko Mokrý"
              date="12.12.2020"
            />
            <RequestCard
              name="Janko Mokrý"
              date="12.12.2020"
            />
          </div>
        </div>

        <div className="w-full xl:w-1/3 px-3">
          <p className="text-xl font-semibold mb-4">Najnovšie vstupy</p>

          <div className="w-full bg-white border-0 rounded-lg p-4 mb-8 xl:mb-0 shadow-lg">
            <EntryCard
              title="Ab123CD"
              date="12.12.2020"
            />
            <EntryCard
              title="Mestká polícia"
              date="12.12.2020"
            />

            <EntryCard
              title="AA00000"
              date="12.12.2020"
            />
          </div>
        </div>

        <div className="w-full xl:w-1/3 px-3">
          <p className="text-xl font-semibold mb-4">Najnovšie vymáhania</p>
          <div className="w-full bg-white border-0 rounded-lg p-4 shadow-lg">

            <PaymentCard
              name="Janko Mokrý"
              date="12.12.2020"
              price="74.99"
            />

            <PaymentCard
              name="Jožko Mrkvička"
              date="03.03.2020"
              price="14.95"

            />

            <PaymentCard
              name="Peter Zelený"
              date="12.02.2020"
              price="44.99"
            />

            <PaymentCard
              name="jana Veselá"
              date="17.02.2020"
              price="47.99" />
          </div>
        </div>

      </div>

    </div >
  )
}

Index.getLayout = function getLayout(page: any) {
  return <AppLayout>{page}</AppLayout>;
};

function EntryCard({
  title,
  date,
}: {
  title: string;
  date: string;
}) {
  return (
    <div className="w-full bg-gray-100 border rounded-lg flex justify-between items-center px-4 py-2 mb-4">
      <div>
        <p className="font-semibold text-xl">{title}</p>
        <p className='text-sm italic text-gray-500'>{date}</p>
      </div>
      <span className="text-white bg-blue-500 font-semibold text-sm px-4 py-2 rounded-lg">Detail vozidla</span>
    </div>
  );
}


function RequestCard({
  name,
  date
}: {
  name: string;
  date: string;
}) {
  return (
    <div className="w-full bg-gray-100 border rounded-lg flex justify-between items-center px-4 py-2 mb-4">
      <div>
        <p className="font-semibold text-xl">{name}</p>
        <p className='text-sm italic text-gray-500'>{date}</p>
      </div>
      <span className="text-white bg-blue-500 font-semibold text-sm px-4 py-2 rounded-lg">Detail</span>
    </div>
  );
}

function PaymentCard({
  name,
  date,
  price,
}: {
  name: string;
  date: string;
  price: string;
}) {
  return (
    <div className="w-full bg-gray-100 border rounded-lg flex justify-between items-center px-4 py-2 mb-4">
      <div>
        <p className="font-semibold text-xl">{name}</p>
        <p className='text-sm italic text-gray-500'>{date}</p>
      </div>
      <span className="text-red-500 font-semibold text-lg">&euro;{price}</span>
    </div>
  );
}