import AppLayout from "@/components/common/Layout";
import React from "react";

enum Type {
    RESIDENT = "Obyvateľ",
    SUPPLY = "Dodávateľ",
    SCHOOL = "Školy cirkev",
}

enum Status {
    APPROVED = "Schválená",
    PENDING = "Nevyriešená",
    REJECTED = "Zamietnutá",
}

const fakeData = [
    {
        name: 'Eva Kvetinová',
        type: Type.RESIDENT,
        status: Status.REJECTED,
        documentCount: 4,
        created: '2023-05-15',
    },
    {
        name: 'Peter Novák',
        type: Type.SCHOOL,
        documentCount: 1,
        status: Status.APPROVED,
        created: '2023-05-01',
    },
    {
        name: 'Mária Hrušková',
        type: Type.RESIDENT,
        documentCount: 2,
        status: Status.PENDING,
        created: '2023-06-10',
    },
    {
        name: 'Andrej Kováč',
        type: Type.SUPPLY,
        documentCount: 7,
        status: Status.PENDING,
        created: '2023-06-25',
    },
    {
        name: 'Jana Zelená',
        type: Type.RESIDENT,
        documentCount: 8,
        status: Status.APPROVED,
        created: '2023-07-12',
    },
    {
        name: 'Ivan Horák',
        type: Type.SCHOOL,
        documentCount: 4,
        status: Status.REJECTED,
        created: '2023-07-29',
    },
    {
        name: 'Katarína Malá',
        type: Type.SUPPLY,
        documentCount: 7,
        status: Status.APPROVED,
        created: '2023-08-05',
    },
    {
        name: 'Miroslav Dubček',
        type: Type.RESIDENT,
        documentCount: 9,
        status: Status.PENDING,
        created: '2023-08-20',
    },
];

export default function Requests() {

    const getBadgeColor = (status: Status) => {
        switch (status) {
            case Status.APPROVED:
                return 'bg-green-500';
            case Status.PENDING:
                return 'bg-blue-500';
            case Status.REJECTED:
                return 'bg-red-500';
            default:
                return 'bg-gray-500';
        }
    }

    return (
        <div>
            <nav className="text-sm font-semibold mb-6" aria-label="Breadcrumb">
                <ol className="list-none p-0 inline-flex">
                    <li className="flex items-center text-blue-500">
                        <a href="#" className="text-gray-700">Žiadosti</a>
                    </li>
                </ol>
                <div className="flex flex-wrap mb-20 mt-4">
                    <table className="w-full">
                        <tr>
                            <td className="text-gray-700 py-2 border-b w-1/5 font-bold text-lg">Meno</td>
                            <td className="text-gray-700 py-2 border-b w-1/5 font-bold text-lg">Typ</td>
                            <td className="text-gray-700 py-2 border-b w-1/5 font-bold text-lg">Requests</td>
                            <td className="text-gray-700 py-2 border-b w-1/5 font-bold text-lg">Stav</td>
                            <td className="text-gray-700 py-2 border-b w-1/5 font-bold text-lg">Vytvorené</td>
                            <td className="text-gray-700 py-2 border-b w-1/5 font-bold text-lg">Akcia</td>
                        </tr>

                        {
                            fakeData.map((item, index) => {
                                return (
                                    <tr key={`table-item-${index}`}>
                                        <td className="text-gray-700 text-left py-4 border-b border-gray-200">{item.name}</td>
                                        <td className="text-gray-700 text-left py-4 border-b border-gray-200">{item.type}</td>
                                        <td className="text-gray-700 text-left py-4 border-b border-gray-200">{item.documentCount}</td>
                                        <td className="text-gray-700 text-left py-4 border-b border-gray-200">
                                            <span className={`${getBadgeColor(item.status)} p-2 rounded-full text-white text-xs`}>{item.status}</span>
                                        </td>
                                        <td className="text-gray-700 text-left py-4 border-b border-gray-200">{item.created}</td>
                                        <td className="text-gray-700 text-left py-4 border-b border-gray-200">
                                            <div className="flex flex-row gap-4">
                                                <Button text="Zobraziť" color="blue" />
                                                <Button text="Schváliť" color="green" />
                                                <Button text="Zamietnuť" color="red" />
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </table>
                </div>

            </nav>
        </div>
    )
}

function Button({
    text,
    color
}: {
    text: string,
    color: "red" | "green" | "blue"
}) {
    let classColor;
    let classHoverColor;
    switch (color) {
        case "red":
            classColor = "bg-red-500";
            classHoverColor = "bg-red-700";
            break;
        case "green":
            classColor = "bg-green-500";
            classHoverColor = "bg-green-700";
            break;
        case "blue":
            classColor = "bg-blue-500";
            classHoverColor = "bg-blue-700";
            break;
        default:
            classColor = "bg-gray-500";
            classHoverColor = "bg-gray-700";
            break;
    }

    return (
        <button className={`${classColor} hover:${classHoverColor} text-white font-bold py-1 px-2 rounded text-sm`}>
            {text}
        </button>
    );
}

Requests.getLayout = function getLayout(page: React.ReactElement) {
    return (
        <AppLayout>
            {page}
        </AppLayout>
    )
}