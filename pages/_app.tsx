import store from "@/store";
import "../src/app/globals.css";

import { Provider } from "react-redux";

export default function MyApp({ Component, pageProps }: {
    Component: any;
    pageProps: any;
}) {
    // Use the layout defined at the page level, if available
    const getLayout = Component.getLayout || ((page: any) => page);

    return (
        <>

            <Provider store={store}>
                {getLayout(<Component {...pageProps} />)}
            </Provider>
        </>
    );
}
