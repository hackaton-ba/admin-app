import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";

const reducer = combineReducers({
    // [baseSplitApi.reducerPath]: baseSplitApi.reducer,
});

const store = configureStore({
    // reducer: reducer,
    reducer: {},
    middleware: (getDefaultMiddleware: any) =>
        getDefaultMiddleware().concat(
            // baseSplitApi.middleware
        ),
});

export default store;
