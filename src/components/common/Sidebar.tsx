import React from "react"
import { IconType } from "react-icons";

import { BiSolidDashboard } from "react-icons/bi";
import { SiSimpleanalytics } from "react-icons/si";
import { AiFillControl } from "react-icons/ai";
import { HiDocumentDuplicate } from "react-icons/hi";
import Link from "next/link";

export default function Sidebar() {
    const [sideBarOpen, setSideBarOpen] = React.useState(false);
    return (
        <div className={`w-1/2 md:w-1/3 lg:w-64 fixed md:top-0 md:left-0 h-screen lg:block bg-gray-100 border-r z-30 ${sideBarOpen ? '' : 'hidden'}`} id="main-nav">

            <div className="w-full h-20 border-b flex px-4 items-center mb-8">
                <p className="font-semibold text-3xl text-blue-400 pl-4">LOGO</p>
            </div>

            <div className="mb-4 px-4">
                <p className="pl-4 text-sm font-semibold mb-1">Hlavné</p>

                <MenuItem
                    icon={BiSolidDashboard}
                    text="Domov"
                    active={true}
                    href="/"
                />
                <MenuItem
                    icon={SiSimpleanalytics}
                    text="Štatistiky"
                    active={false}
                    href="/statistiky"
                />
                <MenuItem
                    icon={HiDocumentDuplicate}
                    text="Žiadosti"
                    active={false}
                    href="/ziadosti"
                />

            </div>

            <div className="mb-4 px-4">
                <p className="pl-4 text-sm font-semibold mb-1">Ostatné</p>
                <MenuItem
                    icon={AiFillControl}
                    text="Ovládanie"
                    active={false}
                    href="#"
                />
            </div>

        </div>

    );
}


function MenuItem({ icon, text, active, href }: {
    icon: IconType,
    text: string,
    active: boolean,
    href: string
}) {
    const Icon = icon;
    return (
        <Link href={href}>
            <div className="w-full flex items-center text-blue-400 h-10 pl-4 hover:bg-gray-200 rounded-lg cursor-pointer">
                <Icon className="h-5 w-5 fill-current mr-2" />
                <span className="text-gray-700">{text}</span>
            </div>
        </Link>
    );
}