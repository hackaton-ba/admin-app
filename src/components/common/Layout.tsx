import React from 'react';
import Sidebar from './Sidebar';
import TopHeader from './TopHeader';
import Footer from './Footer';

export default function AppLayout({
    children
}: {
    children: React.ReactNode;
}) {
    return (
        <div className="leading-normal tracking-normal" id="main-body">
            <div className="flex flex-wrap">

                <Sidebar />

                <div className="w-full bg-gray-100 pl-0 lg:pl-64 min-h-screen" >
                    <TopHeader />

                    <div className="p-6 bg-gray-100 mb-20">
                        {children}

                    </div>

                    <Footer />

                </div>
            </div>
        </div>
    )
}