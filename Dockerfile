# STEP 1
# Install dependencies only when needed
FROM node:20.5-alpine AS deps
ENV NODE_ENV=production
WORKDIR /app
COPY . .
RUN npm ci && npm run build && npm prune --production

# STEP 2
# Rebuild the source code only when needed
FROM node:20.5-alpine

WORKDIR /app

COPY . .
COPY --from=deps /app/node_modules ./node_modules
COPY --from=deps /app/package.json ./package.json
COPY --from=deps /app/public ./public
COPY --from=deps /app/.next ./.next

EXPOSE 3000

CMD ["npm", "run", "start"]
